package com.kk00515.requests.json;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({"request","basket","funds","purchases"})
public class Customer extends User {
	
	
	@JsonProperty
	private String request;
	@JsonProperty
	private List<Product> basket = new ArrayList<Product>();
	@JsonProperty
	private double funds;
	@JsonProperty
	private List<Purchase> purchases = new ArrayList<Purchase>();
	
	
	

	public String getRequest() {
		return request;
	}

	public void setRequest(String request) {
		this.request = request;
	}

	public List<Product> getBasket() {
		return basket;
	}

	public void setBasket(List<Product> basket) {
		this.basket = basket;
	}


	public double getFunds() {
		return funds;
	}

	public void setFunds(double funds) {
		this.funds = funds;
	}

	public List<Purchase> getPurchases() {
		return purchases;
	}

	public void setPurchases(List<Purchase> purchases) {
		this.purchases = purchases;
	}
	
	
	

}
