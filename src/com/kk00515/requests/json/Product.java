package com.kk00515.requests.json;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Product {
	
	@JsonProperty
	private String request;
	@JsonProperty
	private int id;
	@JsonProperty
	private String name;
    @JsonProperty
	private String description;
	@JsonProperty
    private double price;
	
	
	
	public String getRequest() {
		return request;
	}
	public void setRequest(String request) {
		this.request = request;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	@Override
	public String toString() {
		return "Product [name: " + name + " price:" + price + "]";
	}
	
	
	
	
	
	

}
