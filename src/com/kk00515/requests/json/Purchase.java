package com.kk00515.requests.json;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Purchase {
	
	@JsonProperty
	private int purchaseId;
	@JsonProperty
	private String productName;
	@JsonProperty
	private double price;
	
	public int getPurchaseId() {
		return purchaseId;
	}
	public void setPurchaseId(int purchaseId) {
		this.purchaseId = purchaseId;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}

	
	
	
}
