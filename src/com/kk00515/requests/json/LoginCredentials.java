package com.kk00515.requests.json;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({"request","email","password"})
public class LoginCredentials {
	
	@JsonProperty
	private String request;
	@JsonProperty
	private String email;
	@JsonProperty
	private String password;
	
	public String getRequest() {
		return request;
	}

	public void setRequest(String request) {
		this.request = request;
	}
	

	public String getEmail() {
		return email;
	}

	public String getPassword() {
		return password;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}

	
	
	
	

}
