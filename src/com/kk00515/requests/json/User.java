package com.kk00515.requests.json;

import com.fasterxml.jackson.annotation.JsonProperty;

public class User {
	
	@JsonProperty
	protected String username;
	@JsonProperty
	protected String password;
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	

}
