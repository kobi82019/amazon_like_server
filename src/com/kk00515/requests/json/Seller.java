package com.kk00515.requests.json;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({"request","username","password","basket","funds","purchases"})
public class Seller  {
	
	@JsonProperty
	private String username;
	@JsonProperty
	private String password;
	@JsonProperty
	private String request;
	@JsonProperty
	private List<Product> productsListed = new ArrayList<Product>();
	@JsonProperty
	private double funds;
	@JsonProperty
	private List<Purchase> soldProducts = new ArrayList<Purchase>();
	
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getRequest() {
		return request;
	}
	public void setRequest(String request) {
		this.request = request;
	}
	public List<Product> getProductsListed() {
		return productsListed;
	}
	public void setProductsListed(List<Product> productsListed) {
		this.productsListed = productsListed;
	}
	public double getFunds() {
		return funds;
	}
	public void setFunds(double funds) {
		this.funds = funds;
	}
	public List<Purchase> getSoldProducts() {
		return soldProducts;
	}
	public void setSoldProducts(List<Purchase> soldProducts) {
		this.soldProducts = soldProducts;
	}
	
	
		
}
