package com.dbHandler;

import java.util.Collections;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.hibernate.Customer;
import com.hibernate.LoginCredentials;
import com.hibernate.Product;
import com.hibernate.Purchase;
import com.hibernate.Seller;

public class CustomerDbHandler {
	
	private SessionFactory sessionFactory;
	private Session session;
	
	public CustomerDbHandler(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	public boolean checkLogin(LoginCredentials login) {
		boolean loginCorrect = false;
		session = sessionFactory.openSession();
		session.beginTransaction();
		try {
			LoginCredentials user = (LoginCredentials)session.get(LoginCredentials.class,login.getEmail());
			if (user.getEmail().equals(login.getEmail())) {
				if (user.getPassword().equals(login.getPassword())) {
				loginCorrect = true;
				}
			}
		}catch (Exception e ) {
			System.out.println(e.toString());
		}
		session.close();
		return loginCorrect;
	}
	
	public List<Product> getAvailableProducts() {
		session = sessionFactory.openSession();
		session.beginTransaction();
		List<Product> products = session.createCriteria(Product.class).list();
		session.close();
		if (products !=null && products.size()!=0) {
			return products;
		}
		return Collections.emptyList();
	}
	
	public Customer getCustomer(String username) {
		Customer customer = null;
		session = sessionFactory.openSession();
		session.beginTransaction();
		customer = session.get(Customer.class, username);
		session.close();
		return customer;
	}
	
	public void savePurchase(Purchase purchase) {
		session = sessionFactory.openSession();
		session.beginTransaction();
		if (session.get(Purchase.class, purchase.getPurchaseId())==null) {
			session.save(purchase);
		}
		session.getTransaction().commit();
		session.close();
	}
	
	public void saveProduct(Product product) {
		session = sessionFactory.openSession();
		session.beginTransaction();
		if (session.get(Product.class, product.getId())==null) {
			session.save(product);
		}
		session.getTransaction().commit();
		session.close();
	}
	
	public void updateSeller(Seller seller) {
		Seller sellerInDb = null;
		session = sessionFactory.openSession();
		session.beginTransaction();
		sellerInDb = session.get(Seller.class, seller.getUsername());
		session.delete(sellerInDb);
		session.save(seller);
		session.getTransaction().commit();
		session.close();
	}
	
	public void updateCustomer(Customer customer) {
		Customer customerInDb = null;
		session = sessionFactory.openSession();
		session.beginTransaction();
		customerInDb = session.get(Customer.class, customer.getUsername());
		session.delete(customerInDb);
		session.save(customer);
		session.getTransaction().commit();
		session.close();
	}
	
	public int getHighestPurchaseID(int id) {
		int highestID = id;
		session = sessionFactory.openSession();
		session.beginTransaction();
		List<Purchase> purchases = session.createCriteria(Purchase.class).list();
		if (purchases!=null) {	
			for(Purchase purchase:purchases) {
				if(purchase.getPurchaseId()>=highestID) {
					highestID = purchase.getPurchaseId();
				}
			}
		}
		
		session.close();
		return highestID;
	}
	
	public int getHighestProductID(int id) {
		int highestID = id;
		session = sessionFactory.openSession();
		session.beginTransaction();
		List<Product> listedProducts = session.createCriteria(Product.class).list();
		if (listedProducts!=null) {	
			for(Product product:listedProducts) {
				if(product.getId()>=highestID) {
					highestID = product.getId();
				}
			}
		}
		
		session.close();
		return highestID;
	}
	
	
	public boolean registerCustomer(LoginCredentials login) {
		boolean registered = false;
		if (getCustomer(login.getEmail())==null) {
			Customer customer = new Customer();
			customer.setUsername(login.getEmail());
			customer.setFunds(0);
			session = sessionFactory.openSession();
			session.beginTransaction();
			session.save(login);
			session.save(customer);
			session.getTransaction().commit();
			registered = true;
		}
		
		return registered;
	}
	
	public Seller getSeller(String username) {
		Seller sellerInDb = null;
		session = sessionFactory.openSession();
		session.beginTransaction();
		sellerInDb = session.get(Seller.class, username);
		session.close();
		return sellerInDb;
	}
}
