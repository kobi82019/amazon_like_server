package com.hibernate;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.tool.hbm2ddl.SchemaExport;

import com.fasterxml.classmate.AnnotationConfiguration;

public class TestHibernate {

	public static void main(String[] args) {
		
		Configuration config = new Configuration();
		config.addAnnotatedClass(LoginCredentials.class);
		config.addAnnotatedClass(Product.class);
		config.addAnnotatedClass(Purchase.class);
		config.addAnnotatedClass(Customer.class);
		config.addAnnotatedClass(Seller.class);

		config.configure("hibernate.cfg.xml");
	
		/**
		
		Customer customer = new Customer();
		customer.setUsername("kobi82019@gmail.com");
		customer.setFunds(21.00);
	
		*/
	
		
		Seller seller = new Seller();
		seller.setFunds(0);
		seller.setUsername("bobBlogg@gmail.com");
		seller.setPassword("password");
		List<Product> products = new ArrayList<Product>();
		
		
		
		SessionFactory sessionFactory = config.buildSessionFactory();
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		
		Seller dbSeller = session.get(Seller.class,"bobBlogg@gmail.com");
		session.delete(dbSeller);
		products.add(session.get(Product.class,1));
		seller.setProductsListed(products);
		session.save(seller);
		session.getTransaction().commit();
		session.close();
		
		
		
		/**
		Customer customer = (Customer)session.get(Customer.class, "kobi82019@gmail.com");
		System.out.println(customer.getUsername());
		System.out.println(customer.getBasket().size());
		*/
		
			
		
		
	
		
		
		
		
		
	}

}
