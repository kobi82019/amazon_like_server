package com.hibernate;
import java.util.ArrayList;
import java.util.List;

import com.kk00515.requests.json.Customer;
import com.kk00515.requests.json.LoginCredentials;
import com.kk00515.requests.json.Product;
import com.kk00515.requests.json.Purchase;
import com.kk00515.requests.json.Seller;

public class HibernateMapper {

	public static com.hibernate.LoginCredentials mapLoginCredientials(LoginCredentials loginCredentials) {
		com.hibernate.LoginCredentials login = new com.hibernate.LoginCredentials();
		login.setEmail(loginCredentials.getEmail());
		login.setPassword(loginCredentials.getPassword());
		return login;
	}
	
	public static Product mapProduct(com.hibernate.Product product) {
		Product jsonProduct = new Product();
		jsonProduct.setId(product.getId());
		jsonProduct.setDescription(product.getDescription());
		jsonProduct.setName(product.getName());
		jsonProduct.setPrice(product.getPrice());
		jsonProduct.setRequest("");
		return jsonProduct;
	}
	
	public static com.hibernate.Product mapProduct(Product product){
		com.hibernate.Product hibernateProduct = new com.hibernate.Product();
		hibernateProduct.setDescription(product.getDescription());
		hibernateProduct.setId(product.getId());
		hibernateProduct.setName(product.getName());
		hibernateProduct.setPrice(product.getPrice());
		return hibernateProduct;
	}
	
	public static Customer mapCustomer(com.hibernate.Customer customer) {
		
		Customer jsonCustomer = new Customer();
		jsonCustomer.setUsername(customer.getUsername());
		jsonCustomer.setFunds(customer.getFunds());
		
		
		List<Purchase> jsonPurchases = new ArrayList<Purchase>();
		List<com.hibernate.Purchase> purchases = customer.getPurchases();
		if (purchases!=null) {
			for (com.hibernate.Purchase purchase : purchases) {
				jsonPurchases.add(HibernateMapper.mapHibernatePurchase(purchase));
			} 
		}
		
		List<com.hibernate.Product> basket = customer.getBasket();
		List<Product> jsonBasket = new ArrayList<Product>();
		if(basket!=null) {
			for (com.hibernate.Product product : basket) {
				jsonBasket.add(HibernateMapper.mapProduct(product));
			}
		}
		
		jsonCustomer.setBasket(jsonBasket);
		jsonCustomer.setPurchases(jsonPurchases);
		
		return jsonCustomer;
	}
	
	public static Purchase mapHibernatePurchase(com.hibernate.Purchase purchase) {
		Purchase jsonPurchase = new Purchase();
		jsonPurchase.setPrice(purchase.getPrice());
		jsonPurchase.setProductName(purchase.getProductName());
		jsonPurchase.setPurchaseId(purchase.getPurchaseId());
		return jsonPurchase;
	}
	
	public static com.hibernate.Purchase mapPurchase(Purchase purchase){
		com.hibernate.Purchase hibernatePurchase = new com.hibernate.Purchase();
		hibernatePurchase.setPrice(purchase.getPurchaseId());
		hibernatePurchase.setProductName(purchase.getProductName());
		hibernatePurchase.setPurchaseId(purchase.getPurchaseId());
		return hibernatePurchase;
	}
	
	public static com.hibernate.Customer mapJsonCustomer(Customer customer){
		com.hibernate.Customer hibernateCustomer = new com.hibernate.Customer();
		List<com.hibernate.Product> basket = new ArrayList<com.hibernate.Product>();
		List<com.hibernate.Purchase> purchases = new ArrayList<com.hibernate.Purchase>();
		if (customer.getBasket()!=null) {
			for (Product product:customer.getBasket()) {
				basket.add(HibernateMapper.mapProduct(product));
			}
		}
		if (customer.getPurchases()!=null) {
			for (Purchase purchase: customer.getPurchases()) {
				purchases.add(HibernateMapper.mapPurchase(purchase));
			}
		}
		hibernateCustomer.setBasket(basket);
		hibernateCustomer.setPurchases(purchases);
		hibernateCustomer.setUsername(customer.getUsername());
		hibernateCustomer.setFunds(customer.getFunds());		
		return hibernateCustomer;
	}
	
	public static Seller mapHibernateSeller(com.hibernate.Seller hibSeller) {
		Seller seller = new Seller();
		seller.setUsername(hibSeller.getUsername());
		seller.setPassword(hibSeller.getPassword());
		seller.setFunds(hibSeller.getFunds());
		
		List<Purchase> jsonPurchases = new ArrayList<Purchase>();
		List<com.hibernate.Purchase> purchases = hibSeller.getSoldProducts();
		if (purchases!=null) {
			for (com.hibernate.Purchase purchase : purchases) {
				jsonPurchases.add(HibernateMapper.mapHibernatePurchase(purchase));
			} 
		}
		
		List<com.hibernate.Product> listedProducts = hibSeller.getProductsListed();
		List<Product> jsonProducts = new ArrayList<Product>();
		if(listedProducts!=null) {
			for (com.hibernate.Product product : listedProducts) {
				jsonProducts.add(HibernateMapper.mapProduct(product));
			}
		}
		
		seller.setProductsListed(jsonProducts);
		seller.setSoldProducts(jsonPurchases);
	
		return seller;
	}
	
	
	public static com.hibernate.Seller mapJsonSeller(Seller seller){
		com.hibernate.Seller hibernateSeller = new com.hibernate.Seller();
		List<com.hibernate.Product> listedProducts = new ArrayList<com.hibernate.Product>();
		List<com.hibernate.Purchase> purchases = new ArrayList<com.hibernate.Purchase>();
		if (seller.getProductsListed()!=null) {
			for (Product product:seller.getProductsListed()) {
				listedProducts.add(HibernateMapper.mapProduct(product));
			}
		}
		if (seller.getSoldProducts()!=null) {
			for (Purchase purchase: seller.getSoldProducts()) {
				purchases.add(HibernateMapper.mapPurchase(purchase));
			}
		}
		hibernateSeller.setProductsListed(listedProducts);
		hibernateSeller.setSoldProducts(purchases);
		hibernateSeller.setUsername(seller.getUsername());
		hibernateSeller.setFunds(seller.getFunds());
		hibernateSeller.setPassword(seller.getPassword());
		return hibernateSeller;
	}
	
	
	
	
}
