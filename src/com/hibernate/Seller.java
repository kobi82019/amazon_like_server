package com.hibernate;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
public class Seller {
	
	@Id
	private String username;
	private String password;
	private double funds;
	
	@OneToMany(fetch = FetchType.EAGER)
	@Fetch(value = FetchMode.SUBSELECT)
	private List<Product> productsListed;
	
	@OneToMany(fetch = FetchType.EAGER)
	@Fetch(value = FetchMode.SUBSELECT)
	private List<Purchase> soldProducts;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public double getFunds() {
		return funds;
	}

	public void setFunds(double funds) {
		this.funds = funds;
	}

	public List<Product> getProductsListed() {
		return productsListed;
	}

	public void setProductsListed(List<Product> productsListed) {
		this.productsListed = productsListed;
	}

	public List<Purchase> getSoldProducts() {
		return soldProducts;
	}

	public void setSoldProducts(List<Purchase> soldProducts) {
		this.soldProducts = soldProducts;
	} 
	
	
	
	

}
