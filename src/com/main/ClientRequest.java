package com.main;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonUnwrapped;

public class ClientRequest {

	@JsonProperty
	private String request;
	
	public ClientRequest(@JsonProperty("request") String request) {
		this.request = request;
	}
	

	public String getRequest() {
		return request;
	}

	public void setRequest(String request) {
		this.request = request;
	}


}
