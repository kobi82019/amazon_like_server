package com.main;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

import com.dbHandler.CustomerDbHandler;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.hibernate.HibernateMapper;
import com.kk00515.requests.json.Customer;
import com.kk00515.requests.json.LoginCredentials;
import com.kk00515.requests.json.Product;
import com.kk00515.requests.json.Purchase;
import com.kk00515.requests.json.Seller;

public class ClientRequestExecutor implements Runnable {

	private Socket clientSocket;
	private ObjectMapper objectMapper;
	private boolean requestFinished;
	private CustomerDbHandler customerDb;
	private int noOfStartBraces = 0;
	private int noOfEndBraces = 0;

	public ClientRequestExecutor(Socket clientSocket, CustomerDbHandler customerDb) throws IllegalArgumentException {
		super();
		if (clientSocket==null || customerDb==null) {
			throw new IllegalArgumentException("ClientSocket or customerDb cannot be null");
		}
		this.clientSocket = clientSocket;
		this.objectMapper = new ObjectMapper();
		this.customerDb = customerDb;
		this.requestFinished = false;
	}

	@Override
	public void run() {
		System.out.println("Running thread");
		String json = "";
		String text = "";
		String clientRequest = "";
		try {
			BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

			while (!requestFinished) {
				while ((text = in.readLine()) != null) {
					json = json + text;
					if ((clientRequest = getRequestType(json)) != null) {
						clientRequest = getClientRequest(clientRequest).getRequest();
					}
					if (endOfJson(text)) {
						break;
					}
				}
				execute(json, clientRequest);
				requestFinished = true;
				System.out.println(json);
			}
			System.out.println("Request Finished");
			clientSocket.close();
		} catch (IOException e) {
			System.out.println(e);
		}
	}

	public void execute(String jsonString, String requestType)
			throws JsonParseException, JsonMappingException, IOException {
		PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
		ObjectWriter ow = objectMapper.writer().withDefaultPrettyPrinter();
		String response = "";
		if (requestType.equals("Login")) {
			LoginCredentials login = objectMapper.readValue(jsonString, LoginCredentials.class);
			boolean loginCorrect = customerDb.checkLogin(HibernateMapper.mapLoginCredientials(login));

			if (loginCorrect) {
				login.setRequest("Correct");
			} else {
				login.setRequest("Incorrect");
			}
			response = ow.writeValueAsString(login);

		}
		
		if(requestType.equals("Register account")) {
			LoginCredentials login = objectMapper.readValue(jsonString, LoginCredentials.class);
			boolean registered = customerDb.registerCustomer(HibernateMapper.mapLoginCredientials(login));
			if (registered) {
				login.setRequest("Registered");
			}else {
				login.setRequest("Invalid");
			}
			response = ow.writeValueAsString(login);
		}

		if (requestType.equals("View")) {
			List<Product> products = new ArrayList<Product>();
			for (com.hibernate.Product product : customerDb.getAvailableProducts()) {
				products.add(HibernateMapper.mapProduct(product));
			}
			response = ow.writeValueAsString(products);

		}

		if (requestType.equals("Get Customer")) {
			Customer customer = objectMapper.readValue(jsonString, Customer.class);
			customer = HibernateMapper.mapCustomer(customerDb.getCustomer(customer.getUsername()));
			response = ow.writeValueAsString(customer);
		}
		
		if (requestType.equals("Add To Basket")) {
			Customer customer = objectMapper.readValue(jsonString, Customer.class);
			customerDb.updateCustomer(HibernateMapper.mapJsonCustomer(customer));
			customer.setRequest("Added To Basket");
			response = ow.writeValueAsString(customer);
		}
		
		if (requestType.equals("Add Product")) {
			Seller seller = objectMapper.readValue(jsonString, Seller.class);
			for (Product product: seller.getProductsListed()) {
				int id = customerDb.getHighestProductID(product.getId());
				id++;
				product.setId(id);
				customerDb.saveProduct(HibernateMapper.mapProduct(product));
			}
			customerDb.updateSeller(HibernateMapper.mapJsonSeller(seller));
			seller.setRequest("Added Product");
			response = ow.writeValueAsString(seller);
		}
		
		if (requestType.equals("Buy products")) {
			Customer customer = objectMapper.readValue(jsonString, Customer.class);
			for (Purchase purchase:customer.getPurchases()) {
				int id = customerDb.getHighestPurchaseID(purchase.getPurchaseId());
				id++;
				purchase.setPurchaseId(id);
				customerDb.savePurchase(HibernateMapper.mapPurchase(purchase));
			}
			customerDb.updateCustomer(HibernateMapper.mapJsonCustomer(customer));
			customer.setRequest("Bought Products");
			response = ow.writeValueAsString(customer);
		}
		
		if(requestType.equals("Add funds")) {
			Customer customer = objectMapper.readValue(jsonString,Customer.class);
			customerDb.updateCustomer(HibernateMapper.mapJsonCustomer(customer));
			customer.setRequest("Added");
			response = ow.writeValueAsString(customer);
		}
		
		if(requestType.equals("Login Seller")) {
			Seller seller = objectMapper.readValue(jsonString, Seller.class);
			com.hibernate.Seller sellerFromDb = customerDb.getSeller(seller.getUsername());
			if (sellerFromDb!=null) {
				seller = HibernateMapper.mapHibernateSeller(sellerFromDb);
				seller.setRequest("Correct");
			}else {
				seller.setRequest("Incorrect");
			}
			
			response = ow.writeValueAsString(seller);
		}
		if(requestType.equals("Get Seller")) {
			Seller seller = objectMapper.readValue(jsonString, Seller.class);
			com.hibernate.Seller sellerFromDb = customerDb.getSeller(seller.getUsername());
			seller = HibernateMapper.mapHibernateSeller(sellerFromDb);
			response = ow.writeValueAsString(seller);
		}
		
		out.println(response.trim());

	}

	private boolean endOfJson(String text) {
		boolean end = false;
		if (text.equals("{")) {
			noOfStartBraces++;
		}
		if (text.equals("}")) {
			noOfEndBraces++;
		}

		if (noOfStartBraces != 0 && (noOfStartBraces == noOfEndBraces)) {
			end = true;
		}

		return end;
	}

	private ClientRequest getClientRequest(String json) {
		ClientRequest clientRequest = null;
		try {
			clientRequest = objectMapper.readValue(json, ClientRequest.class);
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			System.out.println(e.toString());
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			System.out.println(e.toString());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println(e.toString());
		}
		return clientRequest;
	}

	private String getRequestType(String json) {
		String clientRequest = null;
		if (json.indexOf("request") == -1) {
			System.out.println("Request not found yet");
		} else {
			int indexOfRequest = json.indexOf(",");
			clientRequest = json.substring(0, indexOfRequest) + " }";
		}

		return clientRequest;
	}

}
