package com.main;
import java.io.BufferedInputStream;

import com.dbHandler.CustomerDbHandler;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hibernate.Customer;
import com.hibernate.LoginCredentials;
import com.hibernate.Product;
import com.hibernate.Purchase;
import com.hibernate.Seller;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.Scanner;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class Server {

	ObjectMapper objectMapper = new ObjectMapper();
	
	public static void main(String args[]) throws IOException, InterruptedException {
		System.out.println("Starting server");
		ServerSocket clientListener = new ServerSocket(4444);
		
		
		ProcessBuilder pb = new ProcessBuilder("cmd", "/c", "startNetworkServer.bat");
		URL bfile = Server.class.getClassLoader().getResource("db-derby-10.14.1.0-bin/bin");
		File file = new File(bfile.getPath());
		pb.directory(file);
		Process p = pb.start();
		
		System.out.println("Wait 20 seconds for initialisation");
		Thread.sleep(20000);
		
		Configuration config = new Configuration();
		config.addAnnotatedClass(LoginCredentials.class);
		config.addAnnotatedClass(Product.class);
		config.addAnnotatedClass(Purchase.class);
		config.addAnnotatedClass(Customer.class);
		config.addAnnotatedClass(Seller.class);
		config.configure("hibernate.cfg.xml");
		SessionFactory sessionFactory = config.buildSessionFactory();
		CustomerDbHandler customerDbHandler = new CustomerDbHandler(sessionFactory);
		System.out.println("Ready to handle clients");
		
			while (true) {
				try {
					Socket clientSocket = clientListener.accept();
					if (clientSocket!=null) {
						new Thread(new ClientRequestExecutor(clientSocket,customerDbHandler)).start();
						
					}
				} catch (Exception e ) {
					System.out.println(e.toString());
				} 
				
			}
		
		
		
		
		
	}
	
	
	
}
