package com.kk00515.server;

import static org.junit.Assert.*;

import org.junit.Test;

import com.hibernate.HibernateMapper;
import com.kk00515.requests.json.LoginCredentials;

public class HibernateMapperTest {

	
	@Test
	public void mapLoginCredentialsTest() {
		LoginCredentials login = new LoginCredentials();
		login.setEmail("kobi82019@gmail.com");
		login.setPassword("password");
		assertEquals("kobi82019@gmail.com",HibernateMapper.mapLoginCredientials(login).getEmail());
		assertEquals("password",HibernateMapper.mapLoginCredientials(login).getPassword());
	}

}
