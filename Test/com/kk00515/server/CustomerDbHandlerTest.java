package com.kk00515.server;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.junit.Test;

import com.dbHandler.CustomerDbHandler;
import com.hibernate.LoginCredentials;

public class CustomerDbHandlerTest {

	//Make sure database server is started
	
	private CustomerDbHandler customerDbHandler = new CustomerDbHandler(
			new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(LoginCredentials.class).buildSessionFactory());
	
	@Test
	public void checkLoginTest() {
		LoginCredentials login = new LoginCredentials();
		login.setEmail("kobi82019@gmail.com");
		login.setPassword("password");
		assertTrue(customerDbHandler.checkLogin(login));
		
	}
	
	@Test
	public void checkIncorrectLoginTest() {
		LoginCredentials login = new LoginCredentials();
		login.setEmail("kobi82019@gmail.com");
		login.setPassword("absjd");
		assertFalse(customerDbHandler.checkLogin(login));
		
	}
}
